document.addEventListener('DOMContentLoaded', fn, false);


function fn(){

	var rooms = [];
	var firstDoor = null;
	var secondDoor = null;
	var scale = 20;
	var targetId = null;
	var action = null;
	var x_now = null;
	var y_now = null;
	var min_size = 2.5; //prevent 0 height and width
	var DOM = document.getElementById('svgOne');

	var mode = 0;

	var modeDOM = document.querySelectorAll('input[type="radio"]');
	handlerMode();
	function handlerMode(){
		modeDOM.forEach(function(element){
			element.addEventListener('click', changeMode, false);
		})
	}
	

	function changeMode(event){
		mode = event.target.value;
		firstDoor = null;
		secondDoor = null;
	}

	DOM.addEventListener('mousemove', MouseMove, false);
	function Room ( width, height, top, left, surface, color, colisions )
	{
		this.svgns = "http://www.w3.org/2000/svg";
		this.id = rooms.length + 1;
		this.surface = surface * scale;
		this.width = ( width !== undefined ) && ( width !== null ) ? width : Math.sqrt(this.surface / scale) * scale;
		this.height = ( height !== undefined )  && ( height !== null ) ? height : Math.sqrt(this.surface / scale) * scale;
		var rand = 500 - this.width;
		var rand2 = 500 - this.height
		this.top = ( top !== undefined ) && ( top !== null ) ? top : Math.ceil(Math.random() * rand);
		this.left = ( left !== undefined ) && ( left !==  null ) ? left : Math.ceil(Math.random() * rand2);
		this.color = ( color !== undefined ) && ( color !== null ) ? color : "rgb("+ Math.ceil(Math.random() * 255) +","+ Math.ceil(Math.random() * 255) +","+ Math.ceil(Math.random() * 255) +")";
		this.colisions = ( colisions !== undefined) && ( colisions !== null )  ? colisions : [];
		this.intersections = [];
		this.x_start = null;
		this.y_start = null;
		this.createG();
		this.createReactangle();
		preventCollision( this );
		console.log(this);
	}
	Room.prototype.createG = function(){
		this.g = document.createElementNS(this.svgns, 'g');
		DOM.appendChild(this.g);
	}
	Room.prototype.createReactangle = function(){
		this.rectangle = document.createElementNS(this.svgns, "rect");
		this.rectangle.setAttributeNS(null, 'x', this.left);
		this.rectangle.setAttributeNS(null, 'y', this.top);
		this.rectangle.setAttributeNS(null, 'height', this.height);
		this.rectangle.setAttributeNS(null, 'width', this.width);
		this.rectangle.setAttributeNS(null, 'fill', this.color);
		this.rectangle.setAttributeNS(null, 'draggable', true);
		this.rectangle.setAttributeNS(null, 'style', 'position: relative;z-index: 1');
		this.updateCorners();
		this.rectangle.addEventListener('mousedown', this.moveDiv.bind(this), false);
		this.rectangle.addEventListener('mouseup', this.stopMove.bind(this), false);
		this.rectangle.addEventListener('click', this.setRoom.bind(this), false);
		this.createChangeRectangle();
		this.g.appendChild(this.rectangle);
	}
	Room.prototype.createChangeRectangle = function()
	{
		this.rectangle_left = document.createElementNS(this.svgns, "rect");
		this.rectangle_left.setAttributeNS(null, 'x', this.left - 5);
		this.rectangle_left.setAttributeNS(null, 'y', this.top + (this.height  / 2) - 5 );
		this.rectangle_left.setAttributeNS(null, 'height', 10);
		this.rectangle_left.setAttributeNS(null, 'width', 10);
		this.rectangle_left.setAttributeNS(null, 'fill', 'transparent');
		this.rectangle_left.setAttributeNS(null, 'stroke', '#ff00ff');
		this.rectangle_left.addEventListener("mousedown", this.resizeLeft.bind(this), false);
		this.rectangle_left.addEventListener("mouseup", this.stopMove.bind(this), false);
		this.g.appendChild(this.rectangle_left);

		this.rectangle_right = document.createElementNS(this.svgns, "rect");
		this.rectangle_right.setAttributeNS(null, 'x', this.left + this.width - 5);
		this.rectangle_right.setAttributeNS(null, 'y', this.top + (this.height  / 2) - 5 );
		this.rectangle_right.setAttributeNS(null, 'height', 10);
		this.rectangle_right.setAttributeNS(null, 'width', 10);
		this.rectangle_right.setAttributeNS(null, 'fill', 'transparent');
		this.rectangle_right.setAttributeNS(null, 'stroke', '#ff00ff');
		this.rectangle_right.addEventListener("mousedown", this.resizeRight.bind(this), false);
		this.rectangle_right.addEventListener("mouseup", this.stopMove.bind(this), false);
		this.g.appendChild(this.rectangle_right);

		this.rectangle_top = document.createElementNS(this.svgns, "rect");
		this.rectangle_top.setAttributeNS(null, 'x', this.left + (this.width  / 2) - 5);
		this.rectangle_top.setAttributeNS(null, 'y', this.top - 5 );
		this.rectangle_top.setAttributeNS(null, 'height', 10);
		this.rectangle_top.setAttributeNS(null, 'width', 10);
		this.rectangle_top.setAttributeNS(null, 'fill', 'transparent');
		this.rectangle_top.setAttributeNS(null, 'stroke', '#ff00ff');
		this.rectangle_top.addEventListener("mousedown", this.resizeTop.bind(this), false);
		this.rectangle_top.addEventListener("mouseup", this.stopMove.bind(this), false);
		this.g.appendChild(this.rectangle_top);

		this.rectangle_bottom = document.createElementNS(this.svgns, "rect");
		this.rectangle_bottom.setAttributeNS(null, 'x', this.left + (this.width  / 2) - 5);
		this.rectangle_bottom.setAttributeNS(null, 'y', this.top + this.height - 5 );
		this.rectangle_bottom.setAttributeNS(null, 'height', 10);
		this.rectangle_bottom.setAttributeNS(null, 'width', 10);
		this.rectangle_bottom.setAttributeNS(null, 'fill', 'transparent');
		this.rectangle_bottom.setAttributeNS(null, 'stroke', '#ff00ff');
		this.rectangle_bottom.addEventListener("mousedown", this.resizeBottom.bind(this), false);
		this.rectangle_bottom.addEventListener("mouseup", this.stopMove.bind(this), false);
		this.g.appendChild(this.rectangle_bottom);
	}
	Room.prototype.setRoom = function(event){
		if(mode == 1){
			event.preventDefault();
			event.stopPropagation();

			removeMain();

			firstDoor = this;

			if( !canHall(firstDoor) ){
				setMainDoor(firstDoor);
			}

			
			
		}
	}
	Room.prototype.moveDiv = function(event){
		if(mode == 0){
			event.preventDefault();
			event.stopPropagation();
			this.x_start = event.pageX;
			this.y_start = event.pageY;
			this.pos_top = parseInt(this.top);
			this.pos_left = parseInt(this.left);
			targetId = this.id - 1;
			action = 'move';
		}
		
	}
	Room.prototype.resizeLeft = function(event){
		if(mode == 0){
			event.preventDefault();
			event.stopPropagation();
			targetId = this.id - 1;
			action = 'resize-left';
			this.x_start = event.pageX;
			this.y_start = event.pageY;
			this.old_width = this.width;
			this.pos_left = parseInt(this.left);
		}
		
	}
	Room.prototype.resizeRight = function(event){
		if(mode == 0){
			event.preventDefault();
			event.stopPropagation();
			targetId = this.id - 1;
			action = 'resize-right';
			this.x_start = event.pageX;
			this.y_start = event.pageY;
			this.old_width = this.width;
		}
	}
	Room.prototype.resizeTop = function(event){
		if(mode == 0){
			event.preventDefault();
			event.stopPropagation();
			targetId = this.id - 1;
			action = 'resize-top';
			this.x_start = event.pageX;
			this.y_start = event.pageY;
			this.old_height = this.height;
			this.pos_top = parseInt(this.top);
		}
		
	}
	Room.prototype.resizeBottom = function(event){
		if(mode == 0){
			event.preventDefault();
			event.stopPropagation();
			targetId = this.id - 1;
			action = 'resize-bottom';
			this.x_start = event.pageX;
			this.y_start = event.pageY;
			this.old_height = this.height;
		}
		
	}
	Room.prototype.stopMove = function(event){

		action = null;
		targetId = null;
		event.preventDefault();
		event.stopPropagation();


		preventCollision( this );
	}
	Room.prototype.setPosition = function()
	{
		if( this.canSetPosition() ){
			this.top = this.pos_top + y_now - this.y_start;
			this.left = this.pos_left + x_now - this.x_start;
		}
	}
	Room.prototype.canSetPosition = function()
	{
		return  this.pos_top + y_now - this.y_start >= 0 && 
				this.pos_top + y_now - this.y_start + this.height <= 500 &&
				this.pos_left + x_now - this.x_start >= 0 &&
				this.pos_left + x_now - this.x_start + this.width <= 500;
	}
	Room.prototype.canResize = function()
	{
		return  this.corners.leftTop.x >= 0 &&
				this.corners.rightTop.x <= 500 &&
				this.corners.leftBottom.x >= 0 &&
				this.corners.rightBottom.x <= 500 &&
				this.corners.leftTop.y >= 0 &&
				this.corners.rightTop.y <= 500 &&
				this.corners.leftBottom.y >= 0 &&
				this.corners.rightBottom.y <= 500;
	}
	Room.prototype.updatePosition = function(){
		this.rectangle.setAttributeNS(null, 'y', this.top);
		this.rectangle.setAttributeNS(null, 'x', this.left);

		this.rectangle_left.setAttributeNS(null, 'x', this.left - 5);
		this.rectangle_left.setAttributeNS(null, 'y', this.top + (this.height  / 2) - 5 );

		this.rectangle_right.setAttributeNS(null, 'x', this.left + this.width - 5);
		this.rectangle_right.setAttributeNS(null, 'y', this.top + (this.height  / 2) - 5 );

		this.rectangle_top.setAttributeNS(null, 'x', this.left + (this.width  / 2) - 5);
		this.rectangle_top.setAttributeNS(null, 'y', this.top - 5 );

		this.rectangle_bottom.setAttributeNS(null, 'x', this.left + (this.width  / 2) - 5);
		this.rectangle_bottom.setAttributeNS(null, 'y', this.top + this.height - 5 );

		
	}
	Room.prototype.updateCorners = function(){
		this.corners = { 
			leftTop: {x: this.left, y: this.top}, 
			rightTop: {x: this.left + this.width, y: this.top}, 
			leftBottom: {x: this.left, y: this.top + this.height}, 
			rightBottom: {x: this.left + this.width, y: this.top + this.height}
		}
	}
	Room.prototype.setRightWidth = function(){
		if(this.canResize()){
			if(this.width > min_size || this.old_width + x_now - this.x_start >= min_size)
				this.width = this.old_width + x_now - this.x_start;
			else
				this.width = min_size;
			this.height =  this.surface / this.width * scale;
		}
		
	}
	Room.prototype.setLeftWidth = function(){
		if(this.width > min_size || this.old_width + this.x_start - x_now >= min_size)
	    	this.width= this.old_width + this.x_start - x_now;
	    else
	    	this.width = min_size;
	    this.height =  this.surface / this.width * scale;
	    this.left = this.pos_left + (this.old_width - this.width);
	}
	Room.prototype.setBottomWidth = function(){
		if(this.height > min_size || this.old_height + y_now - this.y_start >= min_size)
			this.height = this.old_height + y_now - this.y_start;
		else 
			this.height = min_size;
		this.width = this.surface / this.height * scale;
	}
	Room.prototype.setTopWidth = function(){
		if(this.height > min_size || this.old_height + this.y_start - y_now >= min_size)
    		this.height= this.old_height + this.y_start - y_now;
    	else
			this.height = min_size;
    	this.width =  this.surface / this.height * scale;
		this.top = this.pos_top + ( this.old_height -this.height );
	}
	Room.prototype.updateSize = function(){
		this.rectangle.setAttributeNS(null, 'width', this.width);
		this.rectangle.setAttributeNS(null, 'height', this.height);
		this.rectangle_left.setAttributeNS(null, 'x', this.left - 5);
		this.rectangle_left.setAttributeNS(null, 'y', this.top + (this.height  / 2) - 5 );

		this.rectangle_right.setAttributeNS(null, 'x', this.left + this.width - 5);
		this.rectangle_right.setAttributeNS(null, 'y', this.top + (this.height  / 2) - 5 );

		this.rectangle_top.setAttributeNS(null, 'x', this.left + (this.width  / 2) - 5);
		this.rectangle_top.setAttributeNS(null, 'y', this.top - 5 );

		this.rectangle_bottom.setAttributeNS(null, 'x', this.left + (this.width  / 2) - 5);
		this.rectangle_bottom.setAttributeNS(null, 'y', this.top + this.height - 5 );
	}
	Room.prototype.isContainedWithInObject = function(room){
			if(	
				this.top >= room.top &&
				this.top <= room.top + room.height &&
				this.top + this.height >= room.top &&
				this.top + this.height <= room.top + room.height &&
				room.left >= this.left  &&
				room.left  <=  this.left + this.width &&
				room.left + room.width >= this.left &&
				room.left + room.width <= this.left + this.width
				||
				this.corners.leftTop.x >= room.left && 
            	this.corners.leftTop.x <= room.left + room.width &&
            	this.corners.leftTop.y >= room.top &&
            	this.corners.leftTop.y <= room.top + room.height  
            	||
            	this.corners.rightTop.x >= room.left && 
            	this.corners.rightTop.x <= room.left + room.width  &&
            	this.corners.rightTop.y >= room.top &&
            	this.corners.rightTop.y <= room.top + room.height 
            	||
            	this.corners.leftBottom.x >= room.left && 
            	this.corners.leftBottom.x <= room.left + room.width  &&
            	this.corners.leftBottom.y >= room.top &&
            	this.corners.leftBottom.y <= room.top + room.height
            	||
            	this.corners.rightBottom.x >= room.left && 
            	this.corners.rightBottom.x <= room.left + room.width  &&
            	this.corners.rightBottom.y >= room.top &&
            	this.corners.rightBottom.y <= room.top + room.height
            	||
            	room.corners.leftTop.x >= this.left && 
            	room.corners.leftTop.x <= this.left + this.width &&
            	room.corners.leftTop.y >= this.top &&
            	room.corners.leftTop.y <= this.top + this.height  
            	||
            	room.corners.rightTop.x >= this.left && 
            	room.corners.rightTop.x <= this.left + this.width  &&
            	room.corners.rightTop.y >= this.top &&
            	room.corners.rightTop.y <= this.top + this.height 
            	||
            	room.corners.leftBottom.x >= this.left && 
            	room.corners.leftBottom.x <= this.left + this.width  &&
            	room.corners.leftBottom.y >= this.top &&
            	room.corners.leftBottom.y <= this.top + this.height
            	||
            	room.corners.rightBottom.x >= this.left && 
            	room.corners.rightBottom.x <= this.left + this.width  &&
            	room.corners.rightBottom.y >= this.top &&
            	room.corners.rightBottom.y <= this.top + this.height

    		) return true;
    		return false;
		
		 
	}
	Room.prototype.setNewPosition = function(distX, distY, room){
		if(Math.abs(distX) > Math.abs(distY)) {
	        if (distX > 0) {
	            this.left = room.left - this.width;
	        } else {
	             this.left = room.left + room.width;
	        }
	    } else {
	        if (distY > 0) {
	            this.top = room.top - this.height;
	        } else {
	            this.top = room.top + room.height;
	        }
	    }

	}
	function MouseMove(event){
		x_now = event.pageX;
		y_now = event.pageY;
		switch (action) {
			case 'move':
				while(doorDOM.firstChild){
					doorDOM.removeChild(doorDOM.firstChild);
					rooms[targetId].colisions = [];
				}

				rooms[targetId].setPosition();		
				rooms[targetId].updatePosition();
				rooms[targetId].updateCorners();
				preventCollision(rooms[targetId]);
				
				break;
			case 'resize-right':
				while(doorDOM.firstChild){
					doorDOM.removeChild(doorDOM.firstChild);
					rooms[targetId].colisions = [];
				}
				if(rooms[targetId].canResize()){
					rooms[targetId].setRightWidth();
					rooms[targetId].updateSize();
					rooms[targetId].updateCorners();
				}
				
				break;
			case 'resize-left':
				while(doorDOM.firstChild){
					doorDOM.removeChild(doorDOM.firstChild);
					rooms[targetId].colisions = [];
				}
				
				if(rooms[targetId].canResize()){
					rooms[targetId].setLeftWidth();
					rooms[targetId].updateSize();
					rooms[targetId].updatePosition();
					rooms[targetId].updateCorners();
				}
				break;
			case 'resize-bottom':
				while(doorDOM.firstChild){
					doorDOM.removeChild(doorDOM.firstChild);
					rooms[targetId].colisions = [];
				}
				
				if(rooms[targetId].canResize()){
					rooms[targetId].setBottomWidth();
					rooms[targetId].updateSize();
					rooms[targetId].updateCorners();
				}
				break;
			case 'resize-top':
				while(doorDOM.firstChild){
					doorDOM.removeChild(doorDOM.firstChild);
					rooms[targetId].colisions = [];
				}
				
				if(rooms[targetId].canResize()){
					rooms[targetId].setTopWidth();
					rooms[targetId].updateSize();
					rooms[targetId].updatePosition();
					rooms[targetId].updateCorners();
				}
				break;
			default:
				// statements_def
				break;
		}
		
	}
	function MouseContainedWithInObjec( room ){
		if(	x_now > room.left && 
            	x_now < room.left + room.width &&
            	y_now > room.top &&
            	y_now < room.top + room.height  
            	||
            	x_now > room.left && 
            	x_now < room.left + room.width  &&
            	y_now > room.top &&
            	y_now < room.top + room.height 
            	||
            	x_now > room.left && 
            	x_now < room.left + room.width  &&
            	y_now > room.top &&
            	y_now < room.top + room.height
            	||
            	x_now> room.left && 
            	x_now < room.left + room.width  &&
            	y_now > room.top &&
            	y_now < room.top + room.height
            	

    		) return true;
    		return false;
	}
	var snap = 30;
	function preventCollision(activeObject){
		collision = null;
		rooms.forEach(function(room){
			if(room.id === activeObject.id)
				return ;
			
            if(activeObject.isContainedWithInObject(room) || room.isContainedWithInObject(activeObject)) {
            	var distX = ((room.left + room.width) / 2) - ((activeObject.left + activeObject.width) / 2);
                var distY = ((room.top + room.height) / 2) - ((activeObject.top + activeObject.height) / 2);
              	activeObject.setNewPosition(distX, distY, room);
              	collistion = room.id;
    		}

    		// Snap roomects to each other horizontally

            // If bottom points are on same Y axis
            if(Math.abs((activeObject.top + activeObject.height) - (room.top + room.height)) < snap) {
                // Snap target BL to roomect BR
                if(Math.abs(activeObject.left - (room.left + room.width)) < snap) {
                    activeObject.left = room.left + room.width;
                }

                // Snap target BR to roomect BL
                if(Math.abs((activeObject.left + activeObject.width) - room.left) < snap) {
                    activeObject.left = room.left - activeObject.width;
                }
            }

            // If top points are on same Y axis
            if(Math.abs(activeObject.top - room.top) < snap) {
                // Snap target TL to roomect TR
                if(Math.abs(activeObject.left - (room.left + room.width)) < snap) {
                    activeObject.left = room.left + room.width;
                }

                // Snap target TR to roomect TL
                if(Math.abs((activeObject.left + activeObject.width) - room.left) < snap) {
                    activeObject.left = room.left - activeObject.width;
                }
            }

            // Snap roomects to each other vertically

            // If right points are on same X axis
            if(Math.abs((activeObject.left + activeObject.width) - (room.left + room.width)) < snap) {
                // Snap target TR to roomect BR
                if(Math.abs(activeObject.top - (room.top + room.height)) < snap) {
                    activeObject.top = room.top + room.height;
                }

                // Snap target BR to roomect TR
                if(Math.abs((activeObject.top + activeObject.height) - room.top) < snap) {
                    activeObject.top = room.top - activeObject.height;
                }
            }

            // If left points are on same X axis
            if(Math.abs(activeObject.left - room.left) < snap) {
                // Snap target TL to roomect BL
                if(Math.abs(activeObject.top - (room.top + room.height)) < snap) {
                    activeObject.top = room.top + room.height;
                }

                // Snap target BL to roomect TL
                if(Math.abs((activeObject.top + activeObject.height) - room.top) < snap) {
                    activeObject.top = room.top - activeObject.height;
                }
            }

		});



		

		//if room  still overlap

		var outerAreaLeft = null,
        outerAreaTop = null,
        outerAreaRight = null,
        outerAreaBottom = null;

		rooms.forEach(function(room){
			if(room.id === activeObject.id)
				return ;
		
			
            if(activeObject.isContainedWithInObject(room) || room.isContainedWithInObject(activeObject)) {

        	 	var intersectLeft = null,
                intersectTop = null,
                intersectWidth = null,
                intersectHeight = null,
                intersectSize = null,
                targetLeft = activeObject.left,
                targetRight = targetLeft + activeObject.width,
                targetTop = activeObject.top,
                targetBottom = targetTop + activeObject.height,
                roomLeft = room.left,
                roomRight = roomLeft + room.width,
                roomTop = room.top,
                roomBottom = roomTop + room.height;


            	// Find intersect information for X axis
                if(targetLeft >= roomLeft && targetLeft <= roomRight) {
                    intersectLeft = targetLeft;
                    intersectWidth = room.width - (intersectLeft - roomLeft);

                } else if(roomLeft >= targetLeft && roomLeft <= targetRight) {
                    intersectLeft = roomLeft;
                    intersectWidth = activeObject.width - (intersectLeft - targetLeft);
                }

                // Find intersect information for Y axis
                if(targetTop >= roomTop && targetTop <= roomBottom) {
                    intersectTop = targetTop;
                    intersectHeight = room.height - (intersectTop - roomTop);

                } else if(roomTop >= targetTop && roomTop <= targetBottom) {
                    intersectTop = roomTop;
                    intersectHeight = activeObject.height - (intersectTop - targetTop);
                }

                // Find intersect size (this will be 0 if room are touching but not overlapping)
                if(intersectWidth > 0 && intersectHeight > 0) {
                    intersectSize = intersectWidth * intersectHeight;
                }

                // Set outer snapping area
                if(room.left < outerAreaLeft || outerAreaLeft === null) {
                    outerAreaLeft = room.left;
                }

                if(room.top < outerAreaTop || outerAreaTop === null) {
                    outerAreaTop = room.top;
                }

                if((room.left + room.width) > outerAreaRight || outerAreaRight === null) {
                    outerAreaRight = room.left + room.width;
                }

                if((room.top + room.height) > outerAreaBottom || outerAreaBottom === null) {
                    outerAreaBottom = room.top + room.height;
                }
                // If roomects are intersecting, reposition outside all shapes which touch
                if(intersectSize) {
                    var distX = (outerAreaRight / 2) - ((activeObject.left + activeObject.width) / 2);
                    var distY = (outerAreaBottom / 2) - ((activeObject.top + activeObject.height) / 2);
                    // Set new position
                    activeObject.setNewPosition(distX, distY, room);
                }


    		}
		});
		
		activeObject.updatePosition();
		activeObject.updateCorners();
	}
	
		

	rooms.push(new Room( null, null, null , null , 15));
	rooms.push(new Room( null, null, 130, 130, 10));
	rooms.push(new Room( null, null, 230, 230, 30));
	rooms.push(new Room( null, null, 430, 420, 10));
	rooms.push(new Room( null, null, 330, 50, 10));

	var doorDOM = document.createElementNS("http://www.w3.org/2000/svg", 'g');
	DOM.appendChild(doorDOM);
	var checkButton = document.getElementById('check');
	checkButton.addEventListener('click', check, false);

	function check (){
		while(doorDOM.firstChild)
				doorDOM.removeChild(doorDOM.firstChild);


		rooms.forEach(function(room){
			room.colisions = [];
			room.intersections = [];
			intersectedRooms(room)
			drawDoors(room);
			if( !canHall( room ) )
				room.colisions.push( 0 );

		})
		console.log( rooms );
	}

	function intersectedRooms( target )
	{
		rooms.forEach(function(room){
			if( target.isContainedWithInObject(room) && touchedRooms( target, room ) && isAlreadyCreatedRoom(target.id, room)  )
			{
				setDoor( target, room )
			}
		})
	}

	function isAlreadyCreatedRoom( id, room ){
		return room.colisions.indexOf(id) === -1 ? true : false;
	}


	function touchedRooms( target, room ){
		startLength = target.intersections.length;
		if(  target.top === room.top + room.height ){
			target.intersections.push("top");
		}
		if( target.top + target.height === room.top ){
			target.intersections.push("bottom");
		}
		if( target.left === room.left + room.width ){
			target.intersections.push("left");
		}
		if( target.left + target.width === room.left ){
			target.intersections.push("right");
		}

		return target.intersections.length > startLength ? true : false;
		
	}

	function setDoor( target, room )
	{
		target.colisions.push(room.id);
	}
    
	function canHall(room)
	{
		return room.intersections.includes("top") &&
		 	room.intersections.includes("bottom") &&
		 	room.intersections.includes("left") &&
		 	room.intersections.includes("right");

	}

	function drawDoors(room){
		var colisions = room.colisions;

			if(colisions.length === 0)
				return ;
			



			colisions.forEach(function(door){
				secondRoom = rooms[door - 1];
		  		var line = document.createElementNS(room.svgns, 'line');
		  		line.setAttributeNS(null, 'x1', (room.left + room.width  / 2));
		  		line.setAttributeNS(null, 'y1', (room.top + room.height / 2));
		  		line.setAttributeNS(null, 'x2', (secondRoom.left + secondRoom.width/ 2));
		  		line.setAttributeNS(null, 'y2', (secondRoom.top + secondRoom.height / 2));
		  		line.setAttributeNS(null, 'stroke', '#fff');
		  		line.setAttributeNS(null, 'stroke-width', '5');
		  		line.setAttributeNS(null, 'first-id', room.id);
		  		line.setAttributeNS(null, 'second-id', secondRoom.id);
		  		line.addEventListener('click', removeDoor, false);
		  		doorDOM.appendChild(line);
			});
			
	}

	function setMainDoor( door )
	{
		/*var main = document.createElementNS(door.svgns, 'path');
  		main.setAttributeNS(null, 'id', door.id);
		main.setAttributeNS(null, 'stroke', '#fff');
		main.setAttributeNS(null, 'stroke-width', '2');
		main.setAttributeNS(null, 'd', 'M '+ (door.left + door.width - 30) +' '+ (door.top + door.height - 30) +
										'L' + (door.left + door.width - 30) +'  '+ (door.top + door.height - 50) +
										'L'+ (door.left + door.width - 20) +' '+ (door.top + door.height - 40) +
										'L'+ (door.left + door.width - 10) +' '+ (door.top + door.height - 50) +
										'L'+ (door.left + door.width - 10) +' '+ (door.top + door.height - 30));
		main.setAttributeNS(null, 'fill', 'transparent');
		mainDOM.appendChild(main);*/


		door.rectangle.setAttributeNS(null, 'stroke-width', '5');
		door.rectangle.setAttributeNS(null, 'stroke', '#000');


		door.main = door.id;
		console.log(door.main);
	}

	function removeDoors(){
		rooms.forEach(function( room ){
			room.colisions = [];	
		})
		while(doorDOM.firstChild)
			doorDOM.removeChild(doorDOM.firstChild);
	}
 	
 	function removeMain(){
 		rooms.forEach(function( room ){
			room.main = null;
			room.rectangle.removeAttributeNS(null, 'stroke-width');
			room.rectangle.removeAttributeNS(null, 'stroke');
		})
 	}

	function removeDoor(event){
		fRoom = event.target.getAttribute('first-id');
		sRoom = event.target.getAttribute('second-id');

		var firstId = fRoom - 1;
		var secondId = sRoom - 1;

		var removeIndex = rooms[firstId].colisions.indexOf(parseInt(sRoom));
		var removeTarget = firstId;
		 if(removeIndex == -1){
		 	removeIndex = rooms[secondId].collisions.indexOf(parseInt(fRoom));
		 	removeTarget = secondId;
		 }

		rooms[removeTarget].colisions.splice(removeIndex, 1);
		doorDOM.removeChild(event.target);
		firstDoor = null;
		secondDoor = null;
		console.log(rooms);


	}
	//console.log(rooms);
}
