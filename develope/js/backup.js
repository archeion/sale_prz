// var canvas = document.getElementById('canvas'),
//     ctx = canvas.getContext('2d'),
//     rect = {
//         x: 150,
//         y: 100,
//         w: 300,
//         h: 150
//     },
//     handlesSize = 20,
//     currentHandle = false,
//     drag = false;

// function init() {
//     canvas.addEventListener('mousedown', mouseDown, false);
//     canvas.addEventListener('mouseup', mouseUp, false);
//     canvas.addEventListener('mousemove', mouseMove, false);
// }

// function point(x, y) {
//     return {
//         x: x,
//         y: y
//     };
// }

// function dist(p1, p2) {
//     return Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
// }

// function getHandle(mouse) {
//     if (dist(mouse, point(rect.x, rect.y)) <= handlesSize) return 'topleft';
//     if (dist(mouse, point(rect.x + rect.w, rect.y)) <= handlesSize) return 'topright';
//     if (dist(mouse, point(rect.x, rect.y + rect.h)) <= handlesSize) return 'bottomleft';
//     if (dist(mouse, point(rect.x + rect.w, rect.y + rect.h)) <= handlesSize) return 'bottomright';
//     if (dist(mouse, point(rect.x + rect.w / 2, rect.y)) <= handlesSize) return 'top';
//     if (dist(mouse, point(rect.x, rect.y + rect.h / 2)) <= handlesSize) return 'left';
//     if (dist(mouse, point(rect.x + rect.w / 2, rect.y + rect.h)) <= handlesSize) return 'bottom';
//     if (dist(mouse, point(rect.x + rect.w, rect.y + rect.h / 2)) <= handlesSize) return 'right';
//     return false;
// }

// function mouseDown(e) {
//     if (currentHandle) drag = true;
//     draw();
// }

// function mouseUp() {
//     drag = false;
//     currentHandle = false;
//     draw();
// }

// function mouseMove(e) {
//     var previousHandle = currentHandle;
//     if (!drag) currentHandle = getHandle(point(e.pageX - this.offsetLeft, e.pageY - this.offsetTop));
//     if (currentHandle && drag) {
//         var mousePos = point(e.pageX - this.offsetLeft, e.pageY - this.offsetTop);
//         switch (currentHandle) {
//             case 'topleft':
//                 rect.w += rect.x - mousePos.x;
//                 rect.h += rect.y - mousePos.y;
//                 rect.x = mousePos.x;
//                 rect.y = mousePos.y;
//                 break;
//             case 'topright':
//                 rect.w = mousePos.x - rect.x;
//                 rect.h += rect.y - mousePos.y;
//                 rect.y = mousePos.y;
//                 break;
//             case 'bottomleft':
//                 rect.w += rect.x - mousePos.x;
//                 rect.x = mousePos.x;
//                 rect.h = mousePos.y - rect.y;
//                 break;
//             case 'bottomright':
//                 rect.w = mousePos.x - rect.x;
//                 rect.h = mousePos.y - rect.y;
//                 break;

//             case 'top':
//                 rect.h += rect.y - mousePos.y;
//                 rect.y = mousePos.y;
//                 break;

//             case 'left':
//                 rect.w += rect.x - mousePos.x;
//                 rect.x = mousePos.x;
//                 break;

//             case 'bottom':
//                 rect.h = mousePos.y - rect.y;
//                 break;

//             case 'right':
//                 rect.w = mousePos.x - rect.x;
//                 break;
//         }
//     }
//     if (drag || currentHandle != previousHandle) draw();
// }

// function draw() {
//     ctx.clearRect(0, 0, canvas.width, canvas.height);
//     ctx.fillStyle = 'black';
//     ctx.fillRect(rect.x, rect.y, rect.w, rect.h);
//     if (currentHandle) {
//         var posHandle = point(0, 0);
//         switch (currentHandle) {
//             case 'topleft':
//                 posHandle.x = rect.x;
//                 posHandle.y = rect.y;
//                 break;
//             case 'topright':
//                 posHandle.x = rect.x + rect.w;
//                 posHandle.y = rect.y;
//                 break;
//             case 'bottomleft':
//                 posHandle.x = rect.x;
//                 posHandle.y = rect.y + rect.h;
//                 break;
//             case 'bottomright':
//                 posHandle.x = rect.x + rect.w;
//                 posHandle.y = rect.y + rect.h;
//                 break;
//             case 'top':
//                 posHandle.x = rect.x + rect.w / 2;
//                 posHandle.y = rect.y;
//                 break;
//             case 'left':
//                 posHandle.x = rect.x;
//                 posHandle.y = rect.y + rect.h / 2;
//                 break;
//             case 'bottom':
//                 posHandle.x = rect.x + rect.w / 2;
//                 posHandle.y = rect.y + rect.h;
//                 break;
//             case 'right':
//                 posHandle.x = rect.x + rect.w;
//                 posHandle.y = rect.y + rect.h / 2;
//                 break;
//         }
//         ctx.globalCompositeOperation = 'xor';
//         ctx.beginPath();
//         ctx.arc(posHandle.x, posHandle.y, handlesSize, 0, 6 * Math.PI);
//         ctx.fill();
//         ctx.globalCompositeOperation = 'source-over';
//     }
// }

// init();
// draw();

// function Shape(x, y, w, h, fill) {
//     this.x = x;
//     this.y = y;
//     this.w = w;
//     this.h = h;
//     this.fill = fill;
// }

// // get canvas element.
// var ctx = document.getElementById('canvas');

// ctx.addEventListener('mousemove', function(e){
//     console.log('e.pageX: ',e.pageX - this.offsetLeft);
//     console.log('e.pageY: ',e.pageY - this.offsetTop);
// });

// // check if context exist
// if (ctx.getContext) {

//     var myRect = [];

//     myRect.push(new Shape(10, 0, 25, 25, "#333"));
//     myRect.push(new Shape(0, 40, 39, 25, "#333"));
//     myRect.push(new Shape(0, 80, 100, 25, "#333"));

//     var context = ctx.getContext('2d');
//     for (var i in myRect) {
//         var objRec = myRect[i];
//         console.log('objRec:',objRec);
//         context.fillStyle = objRec.fill;
//         context.fillRect(objRec.x, objRec.y, objRec.w, objRec.h);

//     }

// }

// $(document).ready(function() {

//     window.globalComposite = new GlobalComposite({
//         width: 1360,
//         height: 768,
//         canvas: $('#oko'),
//         image: $('.elipse'),
//     })

// })

// var GlobalComposite = function(param) {
//     var self = this
//     $.extend(self, param)

//     self.methods()
//     self.getContext()
//     self.render()

//     return self
// }

// GlobalComposite.prototype = {
//     methods: function() {
//         var self = this
//         $.extend(self, {
//             clear: function() {
//                 self.context.clearRect(0, 0, self.width, self.height)
//                 return self
//             },
//             getContext: function() {
//                 self.canvas[0].width = self.width
//                 self.canvas[0].height = self.height
//                 self.context = self.canvas[0].getContext('2d')
//                 return self
//             },
//             draw: function(height) {
//                 self.clear()

//                 self.context.beginPath()
//                 self.context.lineJoin = 'miter'
//                 self.context.lineCap = 'butt'
//                 self.context.lineWidth = 1.000000
//                 self.context.fillStyle = 'black'
//                 self.context.moveTo(0, 0)
//                 self.context.lineTo(self.width, 0)
//                 self.context.lineTo(self.width, self.height)
//                 self.context.lineTo(0, self.height)
//                 self.context.lineTo(0, 0)
//                 self.context.fill()

//                 self.context.globalCompositeOperation = self.gco[self.index]
//                 self.context.drawImage(self.image[0], 0, (self.height - height) / 2, self.width, height)

//                 return self
//             },
//             animate: function() {
//                 TweenLite.to({ a: 1 }, 2, {
//                     a: 0,
//                     onUpdate: function() {
//                         var target = $(this)[0].target.a,
//                             scale = self.image.height() * target

//                         self.draw(scale)
//                     }
//                 })
//                 return self
//             },
//             render: function() {
//                 self.animate()
//                 return self
//             },
//         })
//         return self
//     },
// }

(function() {
            console.log('obj');
    return {
        obj: function() {
            console.log('obj');
        }
    }
})();

var room_module = (function(){
    //metody i właściwości prywatne
    var GlobalComposite = function(param) {
        var self = this
        $.extend(self, param)

        self.methods()
        self.getContext()
        self.render()

        return self
    }

    GlobalComposite.prototype = {
        methods: function() {
            var self = this
            $.extend(self, {
                getContext: function() {
                    self.canvas[0].width = self.width || 600
                    self.canvas[0].height = self.height || 600
                    self.ctx = self.canvas[0].getContext('2d')
                    return self
                },
                clear: function() {
                    return self.ctx.clearRect(0, 0, canvas.width, canvas.height)
                },
                draw: function() {
                    self.clear()
                    var x = 150;

                    self.ctx.fillStyle = '#0d5';
                    self.ctx.fillRect(0, 0, 200, 200);

                    self.ctx.fillStyle = '#f00';
                    self.ctx.fillRect(200, 200, 200, 200);
                    console.log('self.ctx: ',self.ctx);
                    // self.ctx.beginPath();
                    // //              X , Y
                    // self.ctx.moveTo(10, 10);
                    // self.ctx.lineTo(10, x);
                    // self.ctx.lineTo(x, x);
                    // self.ctx.lineTo(x, 10);
                    // self.ctx.lineTo(10, 10);
                    // self.ctx.closePath();
                    // // self.ctx.arcTo(150, 10, 130, 100, 100);
                    // self.ctx.strokeStyle = "red";
                    // self.ctx.lineWidth = 8;
                    // self.ctx.stroke();
                    // self.ctx.fillStyle = "blue";
                    // self.ctx.fill();

                    // self.ctx.beginPath();
                    // //              X , Y
                    // self.ctx.moveTo(x*3, 10);
                    // self.ctx.lineTo(x*3, x);
                    // self.ctx.lineTo(x*2, x);
                    // self.ctx.lineTo(x*2, 10);
                    // self.ctx.lineTo(x*3, 10);
                    // self.ctx.closePath();
                    // // self.ctx.arcTo(150, 10, 130, 100, 100);
                    // self.ctx.strokeStyle = "red";
                    // self.ctx.lineWidth = 8;
                    // self.ctx.stroke();
                    // self.ctx.fillStyle = "blue";
                    // self.ctx.fill();

                    return self
                },
                render: function() {
                    self.draw()
                    return self
                },
            })
            return self
        },
    }


    //metody i właściwości publiczne.
    return {
      init: 'initializing ...',
      // x: x,
      test: function(){
        console.log('testing ...');
      },
      save_data: function(){
        console.log('saving ...');
      },
      get_data: function() {
        console.log('fetching data ...');
      },
      _GlobalComposite: GlobalComposite
    }
})();

window.globalComposite = new room_module._GlobalComposite({
    canvas: $('#canvas'),
})

// console.log(room_module.init);
// room_module.test();
// room_module.save_data();

// var config = {cos: 'value1'};
// var def = {key1: 'default1', key2: 'default2', key3: 'default3'};

// $.extend(def, config);
// console.log(def);