(function() {

    var canvas = new fabric.Canvas('canvas'),
        canvasWidth = document.getElementById('canvas').width,
        canvasHeight = document.getElementById('canvas').height,
        grid = 50,
        snap = 0, //Pixels to snap
        noCollisionMode = true,
        threshold = grid * 0.1,
        snapToObjectsMode = true,
        snapToGridMode = true,
        baseScaleValue = 0.00001, //preventing situation when scale === 0
        components = {
            purple: new fabric.Rect({
                id: 1,
                left: 50,
                top: 50,
                width: 100,
                height: 100,
                fill: '#f0e',
                lockScalingFlip: true,
                hasRotatingPoint: false
            }),
            yellow: new fabric.Rect({
                id: 2,
                left: 350,
                top: 50,
                width: 100,
                height: 100,
                fill: '#ff0',
                lockScalingFlip: true,
                hasRotatingPoint: false
            }),
            red: new fabric.Rect({
                id: 3,
                left: 350,
                top: 200,
                width: 100,
                height: 100,
                fill: '#f00',
                lockScalingFlip: true,
                hasRotatingPoint: false
            }),
            green: new fabric.Rect({
                id: 4,
                left: 50,
                top: 200,
                width: 100,
                height: 100,
                fill: '#0f0',
                lockScalingFlip: true,
                hasRotatingPoint: false
            }),
        };
    function roundToGrid(targetValue) { return Math.round(targetValue / grid) * grid; }

    function getTargetGeometry(target) {
        var geometry = {
            top: target.top,
            left: target.left,
            scaleX: target.scaleX,
            scaleY: target.scaleY,
            clientWidth: target.width*target.scaleX,
            clientHeight: target.height*target.scaleY,
            id: target.id
        };
        geometry.right = geometry.left + geometry.clientWidth;
        geometry.bottom = geometry.top + geometry.clientHeight;
        geometry.corners = [
            [ geometry.left, geometry.top ],
            [ geometry.right, geometry.top ],
            [ geometry.right, geometry.bottom ],
            [ geometry.left, geometry.bottom ]
        ];
        return geometry;
    }

    function checkThreshold(source, destination) {
        if(source > destination - threshold && source < destination + threshold) { return true; }
        return false;
    }

    function checkIfObjectAlreadyIntersecting(source, destination) {
        if(source === destination) { return true; }
        return false;
    }

    function checkIfInsideObject(sourceGeometry, destinationGeometry) {
        for(var i = 0; i < sourceGeometry.corners.length; i++) {
            if(
                (sourceGeometry.corners[i][0] >= destinationGeometry.left && sourceGeometry.corners[i][0] <= destinationGeometry.right && sourceGeometry.corners[i][1] >= destinationGeometry.top && sourceGeometry.corners[i][1] <= destinationGeometry.bottom) ||
                (destinationGeometry.corners[i][0] >= sourceGeometry.left && destinationGeometry.corners[i][0] <= sourceGeometry.right && destinationGeometry.corners[i][1] >= sourceGeometry.top && destinationGeometry.corners[i][1] <= sourceGeometry.bottom)
            ) { return true; }
        }
        return false;
    }

    function getDistance(source, destination) { return Math.abs(source - destination); }
    function getScaleRatio(source, destination, metric) { return getDistance(source, destination)/metric; }

    function findIntersectedObjects(geometry, target) {
        var intersectedObjects = [];
        canvas.forEachObject(function(obj) {
            if(obj === target) { return; }
            if(obj.type !== 'rect') { return; }
            var intersectedGeometry = getTargetGeometry(obj);
            if( !checkIfObjectAlreadyIntersecting(geometry.left, intersectedGeometry.left) && checkThreshold(geometry.left, intersectedGeometry.left) && checkIfInsideObject(geometry, intersectedGeometry) ) {
                intersectedObjects.push({
                    target: intersectedGeometry,
                    type: obj.type,
                    edgeVertical: 'left-left',
                });
            }
            if( !checkIfObjectAlreadyIntersecting(geometry.right, intersectedGeometry.right) && checkThreshold(geometry.right, intersectedGeometry.right) && checkIfInsideObject(geometry, intersectedGeometry) ) {
                intersectedObjects.push({
                    target: intersectedGeometry,
                    type: obj.type,
                    edgeVertical: 'right-right',
                });
            }
            if( checkThreshold(geometry.left, intersectedGeometry.right) && checkIfInsideObject(geometry, intersectedGeometry) ) {
                intersectedObjects.push({
                    target: intersectedGeometry,
                    type: obj.type,
                    edgeVertical: 'left-right',
                });
            }
            if( checkThreshold(geometry.right, intersectedGeometry.left) && checkIfInsideObject(geometry, intersectedGeometry) ) {
                intersectedObjects.push({
                    target: intersectedGeometry,
                    type: obj.type,
                    edgeVertical: 'right-left',
                });
            }

            if( !checkIfObjectAlreadyIntersecting(geometry.top, intersectedGeometry.top) && checkThreshold(geometry.top, intersectedGeometry.top) && checkIfInsideObject(geometry, intersectedGeometry) ) {
                intersectedObjects.push({
                    target: intersectedGeometry,
                    type: obj.type,
                    edgeHorizontal: 'top-top',
                });
            }
            if( !checkIfObjectAlreadyIntersecting(geometry.bottom, intersectedGeometry.bottom) && checkThreshold(geometry.bottom, intersectedGeometry.bottom) && checkIfInsideObject(geometry, intersectedGeometry) ) {
                intersectedObjects.push({
                    target: intersectedGeometry,
                    type: obj.type,
                    edgeHorizontal: 'bottom-bottom',
                });
            }
            if( checkThreshold(geometry.top, intersectedGeometry.bottom) && checkIfInsideObject(geometry, intersectedGeometry) ) {
                intersectedObjects.push({
                    target: intersectedGeometry,
                    type: obj.type,
                    edgeHorizontal: 'top-bottom',
                });
            }
            if( checkThreshold(geometry.bottom, intersectedGeometry.top) && checkIfInsideObject(geometry, intersectedGeometry) ) {
                intersectedObjects.push({
                    target: intersectedGeometry,
                    type: obj.type,
                    edgeHorizontal: 'bottom-top',
                });
            }
        });
        console.log(intersectedObjects);
        return intersectedObjects;
    }

    function reactToIntersection(intersection, target, geometry, snapMode, type) {
        //console.log(target);
        //console.log("intersection: " + intersection + " target: " + target + " geometry: " + geometry + " snapMode: " + snapMode + " type: " + type);
        if(snapMode === 'object') {
            for(var i = 0; i < intersection.length; i++) {
                if(type === 'moving') {
                    if(intersection[i].edgeVertical === 'left-right') {
                        target.left = intersection[i].target.right;
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeVertical === 'left-left') {
                        target.left = intersection[i].target.left;
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeVertical === 'right-left') {
                        target.left = intersection[i].target.left - geometry.clientWidth;
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeVertical === 'right-right') {
                        target.left = intersection[i].target.right - geometry.clientWidth;
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeHorizontal === 'top-bottom') {
                        target.top = intersection[i].target.bottom;
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeHorizontal === 'top-top') {
                        target.top = intersection[i].target.top;
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeHorizontal === 'bottom-top') {
                        target.top = intersection[i].target.top - geometry.clientHeight;
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeHorizontal === 'bottom-bottom') {
                        target.top = intersection[i].target.bottom - geometry.clientHeight;
                        geometry = getTargetGeometry(target);
                    }
                } else if(type === 'scaling') {
                    if(intersection[i].edgeVertical === 'left-right') {
                        target.scaleX = baseScaleValue + getScaleRatio(intersection[i].target.right, geometry.right, target.width);
                        target.left = intersection[i].target.right;
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeVertical === 'left-left') {
                        target.scaleX = baseScaleValue + getScaleRatio(intersection[i].target.left, geometry.right, target.width);
                        target.left = intersection[i].target.left;
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeVertical === 'right-left') {
                        target.scaleX = baseScaleValue + getScaleRatio(intersection[i].target.left, target.left, target.width);
                        geometry = getTargetGeometry(target);
                    }
                     if(intersection[i].edgeVertical === 'right-right') {
                        target.scaleX = baseScaleValue + getScaleRatio(intersection[i].target.right, target.left, target.width);
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeHorizontal === 'top-bottom') {
                        target.scaleY = baseScaleValue + getScaleRatio(intersection[i].target.bottom, geometry.bottom, target.height);
                        target.top = intersection[i].target.bottom;
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeHorizontal === 'top-top') {
                        target.scaleY = baseScaleValue + getScaleRatio(intersection[i].target.top, geometry.bottom, target.height);
                        target.top = intersection[i].target.top;
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeHorizontal === 'bottom-top') {
                        target.scaleY = baseScaleValue + getScaleRatio(intersection[i].target.top, geometry.top, target.height);
                        geometry = getTargetGeometry(target);
                    }
                    if(intersection[i].edgeHorizontal === 'bottom-bottom') {
                        target.scaleY = baseScaleValue + getScaleRatio(intersection[i].target.bottom, geometry.top, target.height);
                        geometry = getTargetGeometry(target);
                    }
                }
            }
        } else if(snapMode === 'grid') {
            var snapPoints = {
                top: roundToGrid(geometry.top),
                left: roundToGrid(geometry.left),
                right: roundToGrid(geometry.right),
                bottom: roundToGrid(geometry.bottom)
            };

            if(type === 'moving') {
                var strongerVertical = null,
                    strongerHorizontal = null;
                if(checkThreshold(geometry.top, snapPoints.top)) {
                    target.top = snapPoints.top;
                    strongerVertical = getDistance(geometry.top, snapPoints.top);
                    geometry = getTargetGeometry(target);
                }
                if(checkThreshold(geometry.bottom, snapPoints.bottom)) {
                    if(getDistance(geometry.bottom, snapPoints.bottom) < strongerVertical || strongerVertical === null) {
                        target.top = snapPoints.bottom - geometry.clientHeight;
                        geometry = getTargetGeometry(target);
                    }
                }
                if(checkThreshold(geometry.left, snapPoints.left)) {
                    target.left = snapPoints.left;
                    strongerHorizontal = getDistance(geometry.left, snapPoints.left);
                    geometry = getTargetGeometry(target);
                }
                if(checkThreshold(geometry.right, snapPoints.right)) {
                    if(getDistance(geometry.right, snapPoints.right) < strongerHorizontal || strongerHorizontal === null) {
                        target.left = snapPoints.right - geometry.clientWidth;
                        geometry = getTargetGeometry(target);
                    }
                }
            } else if(type === 'scaling') {
                var ratio = null;
                if(target.__corner === 'ml' && checkThreshold(geometry.left, roundToGrid(geometry.left))) {
                    target.left = roundToGrid(geometry.left);
                    target.scaleX = baseScaleValue + getScaleRatio(target.left, geometry.right, target.width);
                    geometry = getTargetGeometry(target);
                }
                if(target.__corner === 'mr' && checkThreshold(geometry.right, roundToGrid(geometry.right))) {
                    target.scaleX = baseScaleValue + getScaleRatio(target.left, roundToGrid(geometry.right), target.width);
                    geometry = getTargetGeometry(target);
                }
                if(target.__corner === 'mb' && checkThreshold(geometry.bottom, roundToGrid(geometry.bottom))) {
                    target.scaleY = baseScaleValue + getScaleRatio(target.top, roundToGrid(geometry.bottom), target.height);
                    geometry = getTargetGeometry(target);
                }
                if(target.__corner === 'mt' && checkThreshold(geometry.top, roundToGrid(geometry.top))) {
                    target.top = roundToGrid(geometry.top);
                    target.scaleY = baseScaleValue + getScaleRatio(target.top, geometry.bottom, target.height);
                    geometry = getTargetGeometry(target);
                }
                if(target.__corner === 'br') {
                    if(checkThreshold(geometry.bottom, roundToGrid(geometry.bottom))) {
                         ratio = target.scaleX/target.scaleY;
                        target.scaleY = baseScaleValue + getScaleRatio(target.top, roundToGrid(geometry.bottom), target.height);
                        target.scaleX = ratio*target.scaleY;
                        geometry = getTargetGeometry(target);
                    }
                    if(checkThreshold(geometry.right, roundToGrid(geometry.right))) {
                         ratio = target.scaleX/target.scaleY;
                        target.scaleX = baseScaleValue + getScaleRatio(target.left, roundToGrid(geometry.right), target.width);
                        target.scaleY = target.scaleX/ratio;
                        geometry = getTargetGeometry(target);
                    }
                }
                if(target.__corner === 'bl') {
                    if(checkThreshold(geometry.bottom, roundToGrid(geometry.bottom))) {
                         ratio = target.scaleX/target.scaleY;
                        target.scaleY = baseScaleValue + getScaleRatio(target.top, roundToGrid(geometry.bottom), target.height);
                        target.scaleX = ratio*target.scaleY;
                        target.left = geometry.right - target.width*target.scaleX;
                        geometry = getTargetGeometry(target);
                    }
                    if(checkThreshold(geometry.left, roundToGrid(geometry.left))) {
                         ratio = target.scaleX/target.scaleY;
                        target.scaleX = baseScaleValue + getScaleRatio(roundToGrid(geometry.left), geometry.right, target.width);
                        target.scaleY = target.scaleX/ratio;
                        target.left = geometry.right - target.width*target.scaleX;
                        geometry = getTargetGeometry(target);
                    }
                }
                if(target.__corner === 'tl') {
                    if(checkThreshold(geometry.top, roundToGrid(geometry.top))) {
                         ratio = target.scaleX/target.scaleY;
                        target.top = roundToGrid(geometry.top);
                        target.scaleY = baseScaleValue + getScaleRatio(target.top, geometry.bottom, target.height);
                        target.scaleX = ratio*target.scaleY;
                        target.left = geometry.right - target.width*target.scaleX;
                        geometry = getTargetGeometry(target);
                    }
                    if(checkThreshold(geometry.left, roundToGrid(geometry.left))) {
                         ratio = target.scaleX/target.scaleY;
                        target.left = roundToGrid(geometry.left);
                        target.scaleX = baseScaleValue + getScaleRatio(target.left, geometry.right, target.width);
                        target.scaleY = target.scaleX/ratio;
                        target.top = geometry.bottom - target.height*target.scaleY;
                        geometry = getTargetGeometry(target);
                    }
                }
                if(target.__corner === 'tr') {
                    if(checkThreshold(geometry.top, roundToGrid(geometry.top))) {
                         ratio = target.scaleX/target.scaleY;
                        target.top = roundToGrid(geometry.top);
                        target.scaleY = baseScaleValue + getScaleRatio(target.top, geometry.bottom, target.height);
                        target.scaleX = ratio*target.scaleY;
                        geometry = getTargetGeometry(target);
                    }
                    if(checkThreshold(geometry.right, roundToGrid(geometry.right))) {
                         ratio = target.scaleX/target.scaleY;
                        target.scaleX = baseScaleValue + getScaleRatio(target.left, roundToGrid(geometry.right), target.width);
                        target.scaleY = target.scaleX/ratio;
                        target.top = geometry.bottom - target.height*target.scaleY;
                        geometry = getTargetGeometry(target);
                    }
                }
            }
        }
    }

    function snapToObjects(event, type) {
        var geometry = getTargetGeometry(event.target),
            intersection = findIntersectedObjects(geometry, event.target);
            if(noCollisionMode)
                noCollision(event);
        return reactToIntersection(intersection, event.target, geometry, 'object', type);
    }

    function snapToGrid(target, type) {
        var geometry = getTargetGeometry(target);
        return reactToIntersection(null, target, geometry, 'grid', type);
    }


    function handleMovingToGrid(event) { 
       // Sets corner position coordinates based on current angle, width and height
        if(noCollisionMode)
            noCollision(event);

        if(snapToGridMode) 
            { snapToGrid(event.target, 'moving'); } 
    }
    function handleMovingToObjects(event) { 
        if(snapToObjectsMode) { snapToObjects(event, 'moving'); } 
    }
    function handleScalingToGrid(event) { 
        if(snapToGridMode) { snapToGrid(event.target, 'scaling'); } 
    }
    function handleScalingToObjects(event) { 
        if(snapToObjectsMode) { snapToObjects(event, 'scaling'); } 
    }

    function noCollision(event){
        event.target.setCoords();

        // Don't allow objects off the canvas
        if(event.target.getLeft() < snap) {
            event.target.setLeft(0);
        }

        if(event.target.getTop() < snap) {
            event.target.setTop(0);
        }

        if((event.target.getWidth() + event.target.getLeft()) > (canvasWidth - snap)) {
            event.target.setLeft(canvasWidth - event.target.getWidth());
        }

        if((event.target.getHeight() + event.target.getTop()) > (canvasHeight - snap)) {
            event.target.setTop(canvasHeight - event.target.getHeight());
        }

        // Loop through objects
        canvas.forEachObject(function (obj) {
            if (obj === event.target) return;

            // If objects intersect
            if (event.target.isContainedWithinObject(obj) || event.target.intersectsWithObject(obj) || obj.isContainedWithinObject(event.target)) {

                var distX = ((obj.getLeft() + obj.getWidth()) / 2) - ((event.target.getLeft() + event.target.getWidth()) / 2);
                var distY = ((obj.getTop() + obj.getHeight()) / 2) - ((event.target.getTop() + event.target.getHeight()) / 2);

                // Set new position
                findNewPos(distX, distY, event.target, obj);
            }

            // Snap objects to each other horizontally

            // If bottom points are on same Y axis
            if(Math.abs((event.target.getTop() + event.target.getHeight()) - (obj.getTop() + obj.getHeight())) < snap) {
                // Snap target BL to object BR
                if(Math.abs(event.target.getLeft() - (obj.getLeft() + obj.getWidth())) < snap) {
                    event.target.setLeft(obj.getLeft() + obj.getWidth());
                    event.target.setTop(obj.getTop() + obj.getHeight() - event.target.getHeight());
                }

                // Snap target BR to object BL
                if(Math.abs((event.target.getLeft() + event.target.getWidth()) - obj.getLeft()) < snap) {
                    event.target.setLeft(obj.getLeft() - event.target.getWidth());
                    event.target.setTop(obj.getTop() + obj.getHeight() - event.target.getHeight());
                }
            }

            // If top points are on same Y axis
            if(Math.abs(event.target.getTop() - obj.getTop()) < snap) {
                // Snap target TL to object TR
                if(Math.abs(event.target.getLeft() - (obj.getLeft() + obj.getWidth())) < snap) {
                    event.target.setLeft(obj.getLeft() + obj.getWidth());
                    event.target.setTop(obj.getTop());
                }

                // Snap target TR to object TL
                if(Math.abs((event.target.getLeft() + event.target.getWidth()) - obj.getLeft()) < snap) {
                    event.target.setLeft(obj.getLeft() - event.target.getWidth());
                    event.target.setTop(obj.getTop());
                }
            }

            // Snap objects to each other vertically

            // If right points are on same X axis
            if(Math.abs((event.target.getLeft() + event.target.getWidth()) - (obj.getLeft() + obj.getWidth())) < snap) {
                // Snap target TR to object BR
                if(Math.abs(event.target.getTop() - (obj.getTop() + obj.getHeight())) < snap) {
                    event.target.setLeft(obj.getLeft() + obj.getWidth() - event.target.getWidth());
                    event.target.setTop(obj.getTop() + obj.getHeight());
                }

                // Snap target BR to object TR
                if(Math.abs((event.target.getTop() + event.target.getHeight()) - obj.getTop()) < snap) {
                    event.target.setLeft(obj.getLeft() + obj.getWidth() - event.target.getWidth());
                    event.target.setTop(obj.getTop() - event.target.getHeight());
                }
            }

            // If left points are on same X axis
            if(Math.abs(event.target.getLeft() - obj.getLeft()) < snap) {
                // Snap target TL to object BL
                if(Math.abs(event.target.getTop() - (obj.getTop() + obj.getHeight())) < snap) {
                    event.target.setLeft(obj.getLeft());
                    event.target.setTop(obj.getTop() + obj.getHeight());
                }

                // Snap target BL to object TL
                if(Math.abs((event.target.getTop() + event.target.getHeight()) - obj.getTop()) < snap) {
                    event.target.setLeft(obj.getLeft());
                    event.target.setTop(obj.getTop() - event.target.getHeight());
                }
            }
        });

        event.target.setCoords();

        // If objects still overlap

        var outerAreaLeft = null,
        outerAreaTop = null,
        outerAreaRight = null,
        outerAreaBottom = null;

        canvas.forEachObject(function (obj) {
            if (obj === event.target) return;

            if (event.target.isContainedWithinObject(obj) || event.target.intersectsWithObject(obj) || obj.isContainedWithinObject(event.target)) {

                var intersectLeft = null,
                intersectTop = null,
                intersectWidth = null,
                intersectHeight = null,
                intersectSize = null,
                targetLeft = event.target.getLeft(),
                targetRight = targetLeft + event.target.getWidth(),
                targetTop = event.target.getTop(),
                targetBottom = targetTop + event.target.getHeight(),
                objectLeft = obj.getLeft(),
                objectRight = objectLeft + obj.getWidth(),
                objectTop = obj.getTop(),
                objectBottom = objectTop + obj.getHeight();

                // Find intersect information for X axis
                if(targetLeft >= objectLeft && targetLeft <= objectRight) {
                    intersectLeft = targetLeft;
                    intersectWidth = obj.getWidth() - (intersectLeft - objectLeft);

                } else if(objectLeft >= targetLeft && objectLeft <= targetRight) {
                    intersectLeft = objectLeft;
                    intersectWidth = event.target.getWidth() - (intersectLeft - targetLeft);
                }

                // Find intersect information for Y axis
                if(targetTop >= objectTop && targetTop <= objectBottom) {
                    intersectTop = targetTop;
                    intersectHeight = obj.getHeight() - (intersectTop - objectTop);

                } else if(objectTop >= targetTop && objectTop <= targetBottom) {
                    intersectTop = objectTop;
                    intersectHeight = event.target.getHeight() - (intersectTop - targetTop);
                }

                // Find intersect size (this will be 0 if objects are touching but not overlapping)
                if(intersectWidth > 0 && intersectHeight > 0) {
                    intersectSize = intersectWidth * intersectHeight;
                }

                // Set outer snapping area
                if(obj.getLeft() < outerAreaLeft || outerAreaLeft === null) {
                    outerAreaLeft = obj.getLeft();
                }

                if(obj.getTop() < outerAreaTop || outerAreaTop === null) {
                    outerAreaTop = obj.getTop();
                }

                if((obj.getLeft() + obj.getWidth()) > outerAreaRight || outerAreaRight === null) {
                    outerAreaRight = obj.getLeft() + obj.getWidth();
                }

                if((obj.getTop() + obj.getHeight()) > outerAreaBottom || outerAreaBottom === null) {
                    outerAreaBottom = obj.getTop() + obj.getHeight();
                }

                // If objects are intersecting, reposition outside all shapes which touch
                if(intersectSize) {
                    var distX = (outerAreaRight / 2) - ((event.target.getLeft() + event.target.getWidth()) / 2);
                    var distY = (outerAreaBottom / 2) - ((event.target.getTop() + event.target.getHeight()) / 2);

                    // Set new position
                    findNewPos(distX, distY, event.target, obj);
                }
            }
        });
    }

    // grid
    /*for (var i = 0; i < (background.width / grid); i++) {
        background.add(new fabric.Line([i * grid, 0, i * grid, background.height], { stroke: '#ccc', selectable: false }));
        background.add(new fabric.Line([0, i * grid, background.width, i * grid], { stroke: '#000', selectable: false }));
    }*/

    // scene shapes
    for(var i in components) { canvas.add(components[i]); }

    // scene shapes events
    canvas.on('object:moving', handleMovingToGrid);
    canvas.on('object:scaling', handleScalingToGrid);
    canvas.on('object:moving', handleMovingToObjects);
    canvas.on('object:scaling', handleScalingToObjects);

function findNewPos(distX, distY, target, obj) {
    // See whether to focus on X or Y axis
    if(Math.abs(distX) > Math.abs(distY)) {
        if (distX > 0) {
            target.setLeft(obj.getLeft() - target.getWidth());
        } else {
            target.setLeft(obj.getLeft() + obj.getWidth());
        }
    } else {
        if (distY > 0) {
            target.setTop(obj.getTop() - target.getHeight());
        } else {
            target.setTop(obj.getTop() + obj.getHeight());
        }
    }
}

    return;
})();
